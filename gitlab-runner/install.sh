#!/bin/sh

helm upgrade --install --create-namespace --namespace gitlab-runner gitlab-runner -f gitlab-runner.yaml gitlab/gitlab-runner
helm upgrade --install --namespace gitlab-runner sch-gitlab-runner -f gitlab-runner.yaml -f sch-gitlab-runner.yaml gitlab/gitlab-runner
helm upgrade --install --namespace gitlab-runner sch-blint-public-gitlab-runner -f gitlab-runner.yaml -f sch-blint-public-gitlab-runner.yaml gitlab/gitlab-runner
helm upgrade --install --namespace gitlab-runner sch-blint-private-gitlab-runner -f gitlab-runner.yaml -f sch-blint-private-gitlab-runner.yaml gitlab/gitlab-runner
helm upgrade --install --namespace gitlab-runner sch-hsnlab-gitlab-runner -f gitlab-runner.yaml -f sch-hsnlab-gitlab-runner.yaml gitlab/gitlab-runner


